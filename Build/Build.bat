@ECHO OFF
REM "%WINDIR%\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe" ScrewTurnWiki.msbuild /p:VisualStudioVersion=14.0

REM Using MsBuild from Program files folder that supports new C# 6.0 features (nameof, string interpolation, etc.)
"%PROGRAMFILES(x86)%\MSBuild\14.0\Bin\MsBuild.exe" ScrewTurnWiki.msbuild /p:VisualStudioVersion=14.0
